import axios from 'axios';
import { Message } from 'iview';
import store from '../store';
import {getToken, removeToken} from './cookies/auth';
import { router } from '../router';
import Cookies from 'js-cookie';

// 创建axios实例
const service = axios.create({
    // baseURL: process.env.BASE_API, // api的base_url
    timeout: 15000 // 请求超时时间
});

// request拦截器
service.interceptors.request.use(config => {
    if (store.getters.token) {
        config.headers['Authorization'] = 'Bearer ' + getToken(); // 让每个请求携带token--['X-Token']为自定义key 请根据实际情况自行修改
    }
    return config;
}, error => {
    // Do something with request error
    Promise.reject(error);
});

// respone拦截器
service.interceptors.response.use(
    response => {
        const res = response.data;
        if (res.code === 1) {
            Message.error(res.msg);
            return Promise.reject(res);
        }
        return response;
    },
    error => {
        const res = error.response;
        if (res.status === 400 || res.status === 500 || res.status === 503 || res.status === 504) {
            Message.error('服务器异常');
        } if (res.status === 401) {
            if (store.getters.currentPageName === 'login') {
                Message.error(res.data.msg);
            } else {
                removeToken();
                Cookies.remove('access');
                router.push({
                    name: 'login'
                });
            }
        } else {
            Message.error(res.data.msg);
        }
        return Promise.reject(error);
    }
);

export default service;
