import Cookies from 'js-cookie';

const userNameKey = 'cookies_user_name';

export function getUserName () {
    return Cookies.get(userNameKey);
}

export function setUserName (token) {
    return Cookies.set(userNameKey, token);
}

export function removeUserName () {
    return Cookies.remove(userNameKey);
}
