const getters = {
    cachePage: state => state.app.cachePage,
    lang: state => state.app.lang,
    isFullScreen: state => state.app.isFullScreen,
    openedSubmenuArr: state => state.app.openedSubmenuArr,
    menuTheme: state => state.app.menuTheme,
    themeColor: state => state.app.themeColor,
    pageOpenedList: state => state.app.pageOpenedList,
    currentPageName: state => state.app.currentPageName,
    currentPath: state => state.app.currentPath,
    menuList: state => state.app.menuList,
    routers: state => state.app.routers,
    tagsList: state => state.app.tagsList,
    messageCount: state => state.app.messageCount,
    dontCache: state => state.app.dontCache,

    token: state => state.user.token,
    username: state => state.user.username,
    avatar: state => state.user.avatar,
    permissions: state => state.user.permissions,
    roles: state => state.user.roles
};
export default getters;
