/* eslint-disable camelcase */
import Cookies from 'js-cookie';
import { getToken, setToken, removeToken } from '../../utils/cookies/auth';
import request from '../../utils/request';

const user = {
    state: {
        token: getToken(),
        refresh_token: localStorage.refresh_token,
        username: localStorage.username,
        avatar: localStorage.avatar,
        permissions: [],
        roles: []
    },
    mutations: {
        SET_TOKEN: (state, token) => {
            state.token = token;
        },
        SET_REFRESH_TOKEN: (state, rfToken) => {
            state.refresh_token = rfToken;
            localStorage.refresh_token = rfToken;
        },
        SET_USER_NAME: (state, username) => {
            state.username = username;
            localStorage.username = username;
        },
        SET_AVATOR: (state, avatar) => {
            state.avatar = avatar;
            localStorage.avatar = avatar;
        },
        SET_PERMISSIONS: (state, permissions) => {
            state.permissions = permissions;
            localStorage.permissions = permissions;
        },
        SET_ROLES: (state, roles) => {
            state.roles = roles;
            localStorage.roles = roles;
        }
    },
    actions: {

        // 登录
        Login ({ commit }, userInfo) {
            let username = userInfo.username.trim();
            let password = userInfo.password;

            return new Promise((resolve, reject) => {
                request({
                    url: '/auth/form/login',
                    headers: {
                        'Authorization': 'Basic cnlhbjpyeWFuU2VjcmV0'
                    },
                    method: 'post',
                    params: { username, password }
                }).then(response => {
                    const data = response.data;
                    commit('SET_TOKEN', data.access_token);
                    commit('SET_REFRESH_TOKEN', data.refresh_token);
                    setToken(data.access_token);
                    // 设置用户信息
                    commit('SET_USER_NAME', data.username);
                    commit('SET_AVATOR', data.avatar);
                    commit('SET_ROLES', data.roles);
                    Cookies.set('access', 0);
                    resolve();
                }).catch((res) => {
                    reject();
                });
            });
        },
        // 退出登录
        Logout ({ commit }) {
            // 恢复默认样式
            let themeLink = document.querySelector('link[name="theme"]');
            themeLink.setAttribute('href', '');
            // 清空打开的页面等数据，但是保存主题数据
            let theme = '';
            if (localStorage.theme) {
                theme = localStorage.theme;
            }
            localStorage.clear();
            if (theme) {
                localStorage.theme = theme;
            }
            let accesstoken = this.getters.token;
            let refreshToken = this.getters.refresh_token;
            return new Promise((resolve, reject) => {
                request({
                    url: '/auth/form/logout',
                    method: 'delete',
                    params: { accesstoken, refreshToken }
                }).then(response => {
                    removeToken();
                    Cookies.remove('access');
                    resolve();
                });
            });
        },
        // 获取用户基础信息
        GetUserInfo ({ commit }) {
            return new Promise((resolve, reject) => {
                request({
                    url: '/auth/user',
                    method: 'get'
                }).then(response => {
                    const data = response.data.data;
                    commit('SET_USER_NAME', data.username);
                    commit('SET_AVATOR', data.avatar);
                    commit('SET_PERMISSIONS', data.permission);
                    commit('SET_ROLES', data.roles);

                    Cookies.set('access', 0);
                    resolve();
                });
            });
        }
    }
};

export default user;
